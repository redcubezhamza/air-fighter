using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValueHandler : MonoBehaviour
{
    public double moneyConverter;
    public double money;
    public int clickValue;
    public Text moneyText;

    void Start()
    {
        money = 100;
        clickValue = 100;
    }

    void Update()
    {
        CashConverter();

        if(money >= 1000)
        {
            moneyText.text = "Money: " + moneyConverter.ToString("F0") + "K";
        }

        if (money >= 1000000)
        {
            moneyText.text = "Money: " + moneyConverter.ToString("F0") + "M";
        }

        if (money >= 1000000000)
        {
            moneyText.text = "Money: " + moneyConverter.ToString("F5") + "B";
        }

        if (money < 1000)
        {
            moneyText.text = "Money: " + money;
        }
    }

    public void CashConverter()
    {
        if (money >= 1000)
        {
            moneyConverter = money / 1000;
        }

        if (money >= 1000000)
        {
            moneyConverter = money / 1000000;
        }

        if (money >= 1000000000)
        {
            moneyConverter = money / 1000000000;
        }
    }

    public void ClickButton()
    {
        money = money + clickValue;
    }
}
