using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPool : MonoBehaviour
{
    public static BulletPool SharedInstance;

    public GameObject objectToPool1;

    public List<GameObject> pooledObjects1;
    public int amountToPool1;

    public GameObject objectToPool2;

    public List<GameObject> pooledObjects2;
    public int amountToPool2;

    void Awake()
    {
        SharedInstance = this;
    }

    void Start()
    {
        pooledObjects1 = new List<GameObject>();
        pooledObjects2 = new List<GameObject>();

        GameObject tmp1;
        for(int i = 0; i < amountToPool1; i++)
        {
            tmp1 = Instantiate(objectToPool1);
            tmp1.SetActive(false);
            pooledObjects1.Add(tmp1);
        }

        GameObject tmp2;
        for (int i = 0; i < amountToPool2; i++)
        {
            tmp2 = Instantiate(objectToPool2);
            tmp2.SetActive(false);
            pooledObjects2.Add(tmp2);
        }
    }

    public GameObject GetPooledObject1()
    {
        for(int i = 0; i < amountToPool1; i++)
        {
            if(!pooledObjects1[i].activeInHierarchy)
            {
                return pooledObjects1[i];
            }
        }

        return null;
    }

    public GameObject GetPooledObject2()
    {
        for (int i = 0; i < amountToPool2; i++)
        {
            if (!pooledObjects2[i].activeInHierarchy)
            {
                return pooledObjects2[i];
            }
        }

        return null;
    }
}
