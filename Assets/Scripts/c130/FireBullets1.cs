using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullets1 : MonoBehaviour
{
    public static FireBullets1 Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public int bulletsAmount = 10;
    public float startAngle = 90f, endAngle = 270f;
    private Vector2 bulletMoveDirection;

    public int bulletsAmount2 = 10;
    public float startAngle2 = 90f, endAngle2 = 270f;
    private Vector2 bulletMoveDirection2;

    private void Start()
    {
        //InvokeRepeating("Fire", 0, 2);
    }

    public void Fire()
    {
        float angleStep = (endAngle - startAngle) / bulletsAmount;
        float angle = startAngle;

        for(int i = 0; i < bulletsAmount + 1; i++)
        {
            float bulDirX = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180f);
            float bulDirY = transform.position.y + Mathf.Cos((angle * Mathf.PI) / 180f);

            Vector3 bulMoveVector = new Vector3(bulDirX, bulDirY, 0f);
            Vector2 bulDir = (bulMoveVector - transform.position).normalized;

            GameObject bul = BulletPool.SharedInstance.GetPooledObject1();
            if(bul != null)
            {
                bul.transform.position = transform.position;
                bul.transform.rotation = transform.rotation;
                bul.SetActive(true);
                bul.GetComponent<Bullets1>().SetMoveDirection(bulDir);
            }
                
            angle += angleStep;
        }
    }

    public void Fire02()
    {
        float angleStep = (endAngle2 - startAngle2) / bulletsAmount2;
        float angle = startAngle2;

        for (int i = 0; i < bulletsAmount2 + 1; i++)
        {
            float bulDirX = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180f);
            float bulDirY = transform.position.y + Mathf.Cos((angle * Mathf.PI) / 180f);

            Vector3 bulMoveVector = new Vector3(bulDirX, bulDirY, 0f);
            Vector2 bulDir = (bulMoveVector - transform.position).normalized;

            GameObject bul = BulletPool.SharedInstance.GetPooledObject2();
            if (bul != null)
            {
                bul.transform.position = transform.position;
                bul.transform.rotation = transform.rotation;
                bul.SetActive(true);
                bul.GetComponent<Bullets1>().SetMoveDirection(bulDir);
            }

            angle += angleStep;
        }
    }
}
