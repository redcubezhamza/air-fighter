using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullets1 : MonoBehaviour
{
    private Vector2 moveDirection;
    private float moveSpeed = 3.5f;

    private void OnEnable()
    {
        Invoke("Destroy", 2.5f);
    }

    private void Update()
    {
        if (Player.Instance.gameOver == false)
            transform.Translate(moveDirection * moveSpeed * Time.deltaTime);
    }

    public void SetMoveDirection(Vector2 dir)
    {
        moveDirection = dir;
    }

    public void Destroy()
    {
        gameObject.SetActive(false);
    }

    //private void OnDisable()
    //{
    //    CancelInvoke();
    //}
}
