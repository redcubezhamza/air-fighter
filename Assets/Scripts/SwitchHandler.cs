using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class SwitchHandler : MonoBehaviour
{
    //private int switchState = 1;
    public GameObject switchBtn;

    public void Update()
    {
        if (switchBtn.transform.localPosition.x == -47)
        {
            VolumeSaveController.Instance.volumeSlider.interactable = false;
        }
        else
        {
            VolumeSaveController.Instance.volumeSlider.interactable = true;
        }
    }

    public void OnSwitchButtonClicked()
    {
        switchBtn.transform.DOLocalMoveX(-switchBtn.transform.localPosition.x, 0.2f);
        VolumeSaveController.Instance.DoneButton();
        //switchState = Math.Sign(-switchBtn.transform.localPosition.x);
    }
}
