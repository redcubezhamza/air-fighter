using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System;

public class MainMenuScript : MonoBehaviour
{
    public static MainMenuScript Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public AudioSource clickSound;
    public AudioSource BGM;
    public AudioSource OpeningSound;

    public int coinsScore;
    public GameObject coinsScoreDisplay;

    public GameObject skinsPanel;
    public GameObject mapsPanel;

    public GameObject shopPanel;
    public GameObject volumePanel;

    public GameObject leaderboardPanel;
    public GameObject nameWindow;

    public bool settings;
    public bool maps;
    public bool playerSkins;

    public Button settingsButton;
    public Button mapsButton;
    public Button playerSkinsButton;

    public int score;

    public PlayerMainMenu player;

    public static bool initDone = false;

    public GameObject playButton;

    public GameObject skinsSettingsOffButton;

    [SerializeField] int showAds = 0;
    public GameObject AdsDailyLimitExceededPanel;
    public int AdsLimit;

    void Start()
    {
        if (initDone == false)
        {
            initDone = true;
            OpeningSound.Play();
            Invoke("PlayBGM", 07);
        }

        else
        {
            BGM.Play();
        }
        
        score = PlayerPrefs.GetInt("Score");
        
        FreeCoins();
        CoinsScore();
    }

    void PlayBGM()
    {
        BGM.Play();
    }

    public void FreeCoins()
    {
        if (!PlayerPrefs.HasKey("init"))
        {
            PlayerPrefs.SetInt("init", 1);
            PlayerPrefs.SetInt("Coins", 10);
        }
    }

    public void CoinsScore()
    {
        coinsScore = PlayerPrefs.GetInt("Coins");
        coinsScoreDisplay.GetComponent<Text>().text = coinsScore.ToString();

        if (coinsScore < 0)
        {
            coinsScoreDisplay.GetComponent<Text>().text = 0.ToString();
        }
        else
        {
            if (coinsScore < 10)
            {
                coinsScoreDisplay.GetComponent<Text>().text = "0" + coinsScore;
            }
            else
            {
                coinsScoreDisplay.GetComponent<Text>().text = coinsScore.ToString();
            }
        }
    }

    public void PlayGame()
    {
        LevelLoader.Instance.LoadLevel(1);
        SceneManager.LoadScene("Level");
    }

    public void Shop()
    {
        clickSound.Play();
        shopPanel.SetActive(true);
    }

    public void ShopCrossButton()
    {
        clickSound.Play();
        shopPanel.SetActive(false);
    }

    public void SkinSettingsOffButton()
    {
        skinsPanel.SetActive(false);
        mapsPanel.SetActive(false);
        volumePanel.SetActive(false);
        skinsSettingsOffButton.SetActive(false);
        player.transform.DOMoveY(-2.3f, 1);
        playButton.SetActive(true);
        VolumeSaveController.Instance.DoneButton();
    }

    public void PlayerSkins()
    {
        if (!playerSkins)
        {
            clickSound.Play();
            playerSkins = true;
            maps = false;
            settings = false;
            player.transform.DOMoveY(-.2f, .5f);
            skinsSettingsOffButton.SetActive(true);
            playButton.SetActive(false);
            mapsPanel.SetActive(false);
            volumePanel.SetActive(false);
            StartCoroutine(DelaySkinOut());
        }
        else
        {
            clickSound.Play();
            playerSkins = false;
            player.transform.DOMoveY(-2.3f, .5f);
            skinsSettingsOffButton.SetActive(false);
            playButton.SetActive(true);
            skinsPanel.SetActive(false);
            mapsPanel.SetActive(false);
            volumePanel.SetActive(false);
        }
    }

    public void Maps()
    {
        if (!maps)
        {
            clickSound.Play();
            maps = true;
            settings = false;
            playerSkins = false;
            player.transform.DOMoveY(.95f, .5f);
            skinsSettingsOffButton.SetActive(true);
            playButton.SetActive(false);
            skinsPanel.SetActive(false);
            volumePanel.SetActive(false);
            StartCoroutine(DelayMapOut());
        }
        else
        {
            clickSound.Play();
            maps = false;
            player.transform.DOMoveY(-2.3f, .5f);
            skinsSettingsOffButton.SetActive(false);
            playButton.SetActive(true);
            mapsPanel.SetActive(false);
            skinsPanel.SetActive(false);
            volumePanel.SetActive(false);
        }
    }

    public void Settings()
    {
        if (!settings)
        {
            clickSound.Play();
            settings = true;
            playerSkins = false;
            maps = false;
            player.transform.DOMoveY(.95f, .5f);
            skinsSettingsOffButton.SetActive(true);
            playButton.SetActive(false);
            skinsPanel.SetActive(false);
            mapsPanel.SetActive(false);
            StartCoroutine(DelayVolumePanelOut());
        }
        else
        {
            clickSound.Play();
            settings = false;
            player.transform.DOMoveY(-2.3f, .5f);
            skinsSettingsOffButton.SetActive(false);
            playButton.SetActive(true);
            volumePanel.SetActive(false);
            skinsPanel.SetActive(false);
            mapsPanel.SetActive(false);
        }
    }

    public void ExitGame()
    {
        clickSound.Play();
        /*EditorApplication.ExitPlaymode();*/
        Application.Quit();
    }

    public void RewardedAd()
    {
        bool date = true;
        DateTime dt = DateTime.Now;
        
        int AdsLimit = PlayerPrefs.GetInt("AdsLimit");

        if (AdsLimit < 5 && date == true)
        {
            RewardedAds.Instance.RequestRewarded();
            RewardedAds.Instance.ShowRewarded();
            showAds++;
            PlayerPrefs.SetInt("AdsLimit", showAds);

            Debug.Log("Bool " + AdsLimit + DateTime.Now);

            int currentCoins = PlayerPrefs.GetInt("Coins");
            int currentCoins1 = currentCoins + 10000;
            PlayerPrefs.SetInt("Coins", currentCoins1);
        }

        else
        {
            StartCoroutine(AdsLimiTPanel());
        }
    }

    IEnumerator AdsLimiTPanel()
    {
        AdsDailyLimitExceededPanel.SetActive(true);
        yield return new WaitForSeconds(2);
        AdsDailyLimitExceededPanel.SetActive(false);
    }

    public void LeaderboardBackArrowButton()
    {
        clickSound.Play();
        leaderboardPanel.SetActive(false);
        nameWindow.gameObject.SetActive(false);
    }

    public void UpdateScorePlayfab()
    {
        PlayfabManager.instance.SendLeaderboard(score);
    }

    IEnumerator DelaySkinOut()
    {
        yield return new WaitForSeconds(.15f);
        skinsPanel.SetActive(true);
    }

    IEnumerator DelayMapOut()
    {
        yield return new WaitForSeconds(.15f);
        mapsPanel.SetActive(true);
    }

    IEnumerator DelayVolumePanelOut()
    {
        yield return new WaitForSeconds(.15f);
        volumePanel.SetActive(true);
    }
}
