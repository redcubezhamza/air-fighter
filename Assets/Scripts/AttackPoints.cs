using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPoints : MonoBehaviour
{
    public static AttackPoints Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private GameObject player;
    public Quaternion rotation;

    void Start()
    {
        player = GameObject.Find("Player");
    }

    void Update()
    {
        Vector3 direction = player.transform.position - transform.position;
        rotation = Quaternion.LookRotation(direction);
        transform.rotation = rotation;
    }
}
