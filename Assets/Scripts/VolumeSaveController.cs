using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class VolumeSaveController : MonoBehaviour
{
    public static VolumeSaveController Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public Slider volumeSlider = null;
    public Text volumeTextUI = null;

    public GameObject volumePanel;

    float volumeValue;

    public void Start()
    {
        volumeSlider.value = 5;
        volumeValue = volumeSlider.value;

        if (!PlayerPrefs.HasKey("init1"))
        {
            PlayerPrefs.SetFloat("init1", 1);
            PlayerPrefs.SetFloat("VolumeValue", volumeValue);
        }

        LoadValues();
    }

    public void VolumeSlider(float volume)
    {
        volumeTextUI.text = volume.ToString("00");
    }

    public void LoadValues()
    {
        float volumeValue1 = PlayerPrefs.GetFloat("VolumeValue");
        volumeSlider.value = volumeValue1;
        AudioListener.volume = volumeValue1;
    }

    public void DoneButton()
    {
        float volumeValue2 = volumeSlider.value;
        PlayerPrefs.SetFloat("VolumeValue", volumeValue2);
        LoadValues();
    }
}
