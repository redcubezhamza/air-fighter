using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;
using TMPro;
using UnityEngine.Networking;

public class PlayfabManager : MonoBehaviour
{
    public static PlayfabManager instance;
    void Awake()
    {
        PlayFabSettings.TitleId = "ACF9A"; //your title id goes here.
        instance = this;
    }

    public GameObject rowPrefab, nameWindow, leaderboardWindow;
    public Transform rowParent;

    public Transform topPosParent;
    public GameObject firstPosPrefab;
    public GameObject secondPosPrefab;
    public GameObject thirdPosPrefab;
    public GameObject fourthPosPrefab;
    public GameObject fifthPosPrefab;
    public GameObject sixthPosPrefab;
    public GameObject seventhPosPrefab;
    public GameObject eightPosPrefab;
    public GameObject ninthPosPrefab;
    public GameObject tenthPosPrefab;

    public bool LeaderboardButtonPressed = false;
    public Text messageText;

    public InputField nameInput;
    public InputField emailInput;
    public InputField passwordInput;

    public AudioSource clickSound;

    public GameObject networkErrorPanel;

    //public string username = null;

    void Start()
    {
        Login();
    }

    public void LeaderboardButton()
    {
        StartCoroutine(CheckNetworkConnection());
    }

    IEnumerator CheckNetworkConnection()
    {
        UnityWebRequest request = new UnityWebRequest("https://www.google.com");
        yield return request.SendWebRequest();

        if (request.error != null)
        {
            networkErrorPanel.SetActive(true);
            yield return new WaitForSeconds(3);
            networkErrorPanel.SetActive(false);
        }
        else
        {
            leaderboardWindow.SetActive(true);
        }
    }

    public void Login()
    {
        //clickSound.Play();
        var request = new LoginWithCustomIDRequest
        {
            CustomId = SystemInfo.deviceUniqueIdentifier,
            CreateAccount = true,
            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
            {
                GetPlayerProfile = true
            }
        };
        PlayFabClientAPI.LoginWithCustomID(request, OnSuccess, OnError);
    }

    public void OnSuccess(LoginResult result)
    {
        Debug.Log("Login Successfull");
        //SendLeaderboard(GameManager.Instance.score);
        SendLeaderboard(MainMenuScript.Instance.score);
        GetVirtualCurrencies();

        string username = null;

        if (result.InfoResultPayload.PlayerProfile != null)
        {
            username = result.InfoResultPayload.PlayerProfile.DisplayName;
            //PlayerPrefs.SetString("User", username);
        }

        if (username == null)
        {
            nameWindow.SetActive(true);
        }

        //else
        //{
        //    leaderboardWindow.SetActive(true);
        //}
    }

    void OnGetUserInventorySuccess(GetUserInventoryResult result)
    {
        int gems = result.VirtualCurrency["GM"];
        //gemsValueText.text = gems.ToString();
    }

    public void SubmitNameButton()
    {
        var request = new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = nameInput.text,
        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnDisplayNameUpdate, OnError);
    }

    public void OnDisplayNameUpdate(UpdateUserTitleDisplayNameResult result)
    {
        Debug.Log("Updated Display Name");
        nameWindow.SetActive(false);
        //leaderboardWindow.SetActive(true);      
    }

    void OnError(PlayFabError error)
    {
        Debug.Log("Error while logging In/Creating account");
        Debug.Log(error.GenerateErrorReport());
    }

    public void SendLeaderboard(int score)
    {
        var request = new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
                new StatisticUpdate
                {
                    StatisticName = "HighScore",
                    Value = score
                }
            }
        };
        PlayFabClientAPI.UpdatePlayerStatistics(request, OnLeaderboardUpdate, OnError);
    }

    void OnLeaderboardUpdate(UpdatePlayerStatisticsResult result)
    {
        Debug.Log("Leaderboard Updated Successfully");
    }

    public void GetLeaderboard()
    {
        var request = new GetLeaderboardRequest
        {
            StatisticName = "HighScore",
            StartPosition = 0,
            MaxResultsCount = 10
        };
        PlayFabClientAPI.GetLeaderboard(request, OnLeaderboardGet, OnError);
    }

    void OnLeaderboardGet(GetLeaderboardResult result)
    {
        int i = 0;

        //foreach (Transform item in rowParent)
        //{
        //    Destroy(item.gameObject);
        //}

        foreach (Transform item in topPosParent)
        {
            Destroy(item.gameObject);
        }

        foreach (var item in result.Leaderboard)
        {
            if (i == 0)
            {
                GameObject newGO = Instantiate(firstPosPrefab, topPosParent);
                Text[] texts = newGO.GetComponentsInChildren<Text>();

                texts[0].text = (item.Position + 1).ToString();
                texts[1].text = item.DisplayName;
                texts[2].text = item.StatValue.ToString();

                Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));
                //GameObject newGO = Instantiate(firstPosPrefab, topPosParent);
                //Text[] texts = newGO.GetComponentsInChildren<Text>();

                ////texts[0].text = (item.Position + 1).ToString();
                //texts[0].text = item.DisplayName;
                //texts[1].text = item.StatValue.ToString();

                //Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));
            }

            if (i == 1)
            {
                GameObject newGO = Instantiate(secondPosPrefab, topPosParent);
                Text[] texts = newGO.GetComponentsInChildren<Text>();

                texts[0].text = (item.Position + 1).ToString();
                texts[1].text = item.DisplayName;
                texts[2].text = item.StatValue.ToString();

                Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));

                //GameObject newGO = Instantiate(secondPosPrefab, topPosParent);
                //Text[] texts = newGO.GetComponentsInChildren<Text>();

                ////texts[0].text = (item.Position + 1).ToString();
                //texts[0].text = item.DisplayName;
                //texts[1].text = item.StatValue.ToString();

                //Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));
            }

            if (i == 2)
            {
                GameObject newGO = Instantiate(thirdPosPrefab, topPosParent);
                Text[] texts = newGO.GetComponentsInChildren<Text>();

                texts[0].text = (item.Position + 1).ToString();
                texts[1].text = item.DisplayName;
                texts[2].text = item.StatValue.ToString();

                Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));

                //GameObject newGO = Instantiate(thirdPosPrefab, topPosParent);
                //Text[] texts = newGO.GetComponentsInChildren<Text>();

                ////texts[0].text = (item.Position + 1).ToString();
                //texts[0].text = item.DisplayName;
                //texts[1].text = item.StatValue.ToString();

                //Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));
            }

            if (i == 3)
            {
                GameObject newGO = Instantiate(fourthPosPrefab, topPosParent);
                Text[] texts = newGO.GetComponentsInChildren<Text>();

                texts[0].text = (item.Position + 1).ToString();
                texts[1].text = item.DisplayName;
                texts[2].text = item.StatValue.ToString();

                Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));
            }

            if (i == 4)
            {
                GameObject newGO = Instantiate(fifthPosPrefab, topPosParent);
                Text[] texts = newGO.GetComponentsInChildren<Text>();

                texts[0].text = (item.Position + 1).ToString();
                texts[1].text = item.DisplayName;
                texts[2].text = item.StatValue.ToString();

                Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));
            }

            if (i == 5)
            {
                GameObject newGO = Instantiate(sixthPosPrefab, topPosParent);
                Text[] texts = newGO.GetComponentsInChildren<Text>();

                texts[0].text = (item.Position + 1).ToString();
                texts[1].text = item.DisplayName;
                texts[2].text = item.StatValue.ToString();

                Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));
            }

            if (i == 6)
            {
                GameObject newGO = Instantiate(seventhPosPrefab, topPosParent);
                Text[] texts = newGO.GetComponentsInChildren<Text>();

                texts[0].text = (item.Position + 1).ToString();
                texts[1].text = item.DisplayName;
                texts[2].text = item.StatValue.ToString();

                Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));
            }

            if (i == 7)
            {
                GameObject newGO = Instantiate(eightPosPrefab, topPosParent);
                Text[] texts = newGO.GetComponentsInChildren<Text>();

                texts[0].text = (item.Position + 1).ToString();
                texts[1].text = item.DisplayName;
                texts[2].text = item.StatValue.ToString();

                Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));
            }

            if (i == 8)
            {
                GameObject newGO = Instantiate(ninthPosPrefab, topPosParent);
                Text[] texts = newGO.GetComponentsInChildren<Text>();

                texts[0].text = (item.Position + 1).ToString();
                texts[1].text = item.DisplayName;
                texts[2].text = item.StatValue.ToString();

                Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));
            }

            if (i == 9)
            {
                GameObject newGO = Instantiate(tenthPosPrefab, topPosParent);
                Text[] texts = newGO.GetComponentsInChildren<Text>();

                texts[0].text = (item.Position + 1).ToString();
                texts[1].text = item.DisplayName;
                texts[2].text = item.StatValue.ToString();

                Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));
            }
            i++;
        }
    }













    public void RegisterButton()
    {
        if (passwordInput.text.Length < 6)
        {
            messageText.text = "Email too short";
        }

        var request = new RegisterPlayFabUserRequest
        {
            Email = emailInput.text,
            Password = passwordInput.text,
            RequireBothUsernameAndEmail = false
        };
        PlayFabClientAPI.RegisterPlayFabUser(request, OnRegisterSuccess, OnError);
    }

    void OnRegisterSuccess(RegisterPlayFabUserResult result)
    {
        messageText.text = "Registered & Logged in Successfully";
    }

    public void LoginButton()
    {
        var request = new LoginWithEmailAddressRequest
        {
            Email = emailInput.text,
            Password = passwordInput.text
        };
        PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginSuccess, OnError);
    }

    void OnLoginSuccess(LoginResult result)
    {
        messageText.text = "Logged in Success";
        Debug.Log("Successful Login");
    }

    public void ResetPasswordButton()
    {
        var request = new SendAccountRecoveryEmailRequest
        {
            Email = emailInput.text,
            TitleId = "2C40A"
        };
        PlayFabClientAPI.SendAccountRecoveryEmail(request, OnPasswordReset, OnError);
    }

    void OnPasswordReset(SendAccountRecoveryEmailResult result)
    {
        messageText.text = "Password reset mail sent";
    }

    public void GetVirtualCurrencies()
    {
        PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest(), OnGetUserInventorySuccess, OnError);
    }
}