using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePowerup : MonoBehaviour
{
    private float speed = 1f;
    private float yRange = 5f;
    void Update()
    {
        if (Player.Instance.gameOver == false)
        {
            Move();
            EnemyDestroyOutOfBounds();
        }
    }

    public void Move()
    {
        transform.Translate(0, -speed * Time.deltaTime, 0);
    }

    public void EnemyDestroyOutOfBounds()
    {
        if (transform.position.y < -yRange)
            Destroy(gameObject);
    }
}
