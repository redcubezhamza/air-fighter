using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPManager : MonoBehaviour
{
    private string oneMillion = "com.redcubez.airfighter.01M";
    private string fiveMillion = "com.redcubez.airfighter.05M";
    private string tenMillion = "com.redcubez.airfighter.10M";
    private string removeads = "com.redcubez.spaceshooter.removeads";

    public GameObject restoreButton;

    private void Awake()
    {
        if (Application.platform != RuntimePlatform.IPhonePlayer)
        {
            restoreButton.SetActive(false);
        }
    }

    public void OnPurchaseComplete(Product product)
    {
        if (product.definition.id == oneMillion)
        {
            Debug.Log("You have gained 1M Coins");
            int currentCoins = PlayerPrefs.GetInt("Coins");
            int currentCoins1 = currentCoins + 1000000;
            PlayerPrefs.SetInt("Coins", currentCoins1);
        }

        if (product.definition.id == fiveMillion)
        {
            Debug.Log("You have gained 5M Coins");
            int currentCoins = PlayerPrefs.GetInt("Coins");
            int currentCoins1 = currentCoins + 5000000;
            PlayerPrefs.SetInt("Coins", currentCoins1);
        }

        if (product.definition.id == tenMillion)
        {
            Debug.Log("You have gained 10M Coins");
            int currentCoins = PlayerPrefs.GetInt("Coins");
            int currentCoins1 = currentCoins + 10000000;
            PlayerPrefs.SetInt("Coins", currentCoins1);
        }

        if (product.definition.id == removeads)
        {
            Debug.Log("All ads removed!");
        }
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(product.definition.id + "failed because" + failureReason);
    }
}
