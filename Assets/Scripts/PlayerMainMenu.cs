using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMainMenu : MonoBehaviour
{
    [SerializeField] private SkinManager skinManager;

    [SerializeField] private GameObject fireBullets;

    [SerializeField] private Transform attackPoint02;
    [SerializeField] private Transform attackPoint03;
    [SerializeField] private Transform attackPoint09;
    [SerializeField] private Transform attackPoint10;
    [SerializeField] private Transform attackPoint11;
    [SerializeField] private Transform attackPoint12;
    [SerializeField] private Transform attackPoint13;

    [SerializeField] GameObject mainMenuItems;

    void Start()
    {
        InvokeRepeating("PlayerBurst", .2f, .2f);
    }

    void Update()
    {
        GetComponent<SpriteRenderer>().sprite = skinManager.GetSelectedSkin().sprite;
    }

    public void PlayerBurst()
    {
        if (mainMenuItems != null && mainMenuItems.gameObject.activeSelf)
        {
            StartCoroutine(FireBurstPlayerFire(fireBullets, 1, 100));
        }
        else
        {
            Debug.Log("<color=yellow> Main Menu Items not Active </color>");
        }
        
    }

    public IEnumerator FireBurstPlayerFire(GameObject bulletPrefab, int burstSize, float rateOfFire)
    {
        float bulletDelay = 60 / rateOfFire;
        for (int i = 0; i < burstSize; i++)
        {
            Instantiate(fireBullets, attackPoint02.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint03.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint11.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint12.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint13.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint09.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint10.position, Quaternion.identity);

            yield return new WaitForSeconds(bulletDelay);
        }
    }
}
