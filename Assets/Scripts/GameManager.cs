using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public bool isPaused;

    public GameObject pausePanel;
    public GameObject gameOverPanel;
    public GameObject pauseButton;

    public int gem;
    public int gemScore;
    public int coinsScore;
    public int score = 0;
    public int coins = 0;
    public int allcoins = 0;

    public GameObject scoreDisplayAtLevel;
    public GameObject scoreDisplayatGameOver;
    public GameObject gemText;
    public GameObject coinScore;
    public GameObject coinScoreForLevelOnly;
    public GameObject coinScoreForGameOver;
    public GameObject coinScoreForGameOverMultiply3;

    public AudioSource gameMusic;
    public AudioSource pauseSound;
    public AudioSource clickSound;

    public GameObject leaderboardPanel;

    [SerializeField] GameObject scoreSharePanel;
    [SerializeField] Text scoreDisplayAtSharePanel;
    [SerializeField] Text dateText;

    public GameObject player = null;

    public float timeValue;
    public GameObject timerText;

    public AudioSource playerDestroySound;
    public AudioSource hitOnEnemySound;

    public GameObject rateBox;

    public String scoreShareName;
    public GameObject usernameValue;

    public AudioSource fireSound;
    public AudioSource arrowSound;
    public AudioSource diamondSound;

    [SerializeField] int showAds = 0;
    [SerializeField] int coinsAtButton = 0;
    public GameObject AdsDailyLimitExceededPanel;

    private bool coinsThrice = false;

    public static GameManager Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        hitOnEnemySound = fireSound;
        coinsScore = PlayerPrefs.GetInt("Coins");
        InterstitialAds.Instance.RequestInterstitial();
    }

    void Update()
    {
        ScoreDisplayAtLevel();
        ScoreDisplayatGameOver();
        CoinScore();
        Coins();

        PlayerPrefs.SetInt("Score", score);
        PlayerPrefs.Save();

        allcoins = coinsScore + coins + coinsAtButton;
        PlayerPrefs.SetInt("Coins", allcoins);
        PlayerPrefs.Save();

        DisplayTime(timeValue);

        coinScoreForGameOver.GetComponent<Text>().text = coins.ToString();
        coinScoreForGameOverMultiply3.GetComponent<Text>().text = (coins * 3).ToString();
    }

    void DisplayTime(float timeToDisplay)
    {
        if (Player.Instance.gameOver == false)
        {
            timeValue += Time.deltaTime;
            float seconds = Mathf.FloorToInt(timeToDisplay % 60);
            timerText.GetComponent<Text>().text = string.Format("{0:00}", seconds);
        }
    }

    public void FireSound()
    {
         hitOnEnemySound = fireSound;
    }

    public void ArrowSound()
    {
         hitOnEnemySound = arrowSound;
    }

    public void DiamondSound()
    {
         hitOnEnemySound = diamondSound;
    }

    public void Paused()
    {
        pauseSound.Play();
        isPaused = true;
        pausePanel.SetActive(true);
        //pausePanel.GetComponent<Animation>().Play("PausePanel");
        Time.timeScale = 0;
        gameMusic.Pause();
    }

    public void ShareScore()
    {
        clickSound.Play();

        //scoreShareName = PlayerPrefs.GetString("User", scoreShareName);
        //usernameValue.GetComponent<Text>().text = string.Format("{abcdef}", scoreShareName);

        scoreDisplayAtSharePanel.text = scoreDisplayAtLevel.GetComponent<Text>().text;
        DateTime dt = DateTime.Now;

        dateText.text = string.Format("{0}/{1}/{2}", dt.Month, dt.Day, dt.Year);

        scoreSharePanel.SetActive(true);
        StartCoroutine(TakeScreenshotAndShare());
        //StartCoroutine(LoadImageAndShare());

        IEnumerator TakeScreenshotAndShare()
        {
            yield return new WaitForEndOfFrame();

            Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            ss.Apply();

            string filePath = Path.Combine(Application.temporaryCachePath, "Air Fighter.png");
            File.WriteAllBytes(filePath, ss.EncodeToPNG());

            Destroy(ss);

            new NativeShare().AddFile(filePath).SetSubject("Air Fighter").SetText("Can you beat my score in Air Fighter? Give it a try!!").Share();

            scoreSharePanel.SetActive(false);
        }
    }

    public void MainMenuGameOverPanel()
    {
        isPaused = false;
        InterstitialAds.Instance.ShowInterstitial();
        StartCoroutine(Mainmenu());
        IEnumerator Mainmenu()
        {
            yield return new WaitForSeconds(0.5f);
            clickSound.Play();
            Time.timeScale = 1;
            //LevelLoader.Instance.LoadLevel(1);
            
            SceneManager.LoadScene("Main Menu");
        }
    }

    public void Continue01GemButton()
    {
        Player.Instance.gameOver = false;
        Player.Instance.liveScoreValue = 03;
        Player.Instance.CancelInvokes();

        clickSound.Play();
        if (gemScore >= 1)
        {
            gemScore -= 1;
            player.SetActive(true);
            gameMusic.UnPause();
            pauseButton.SetActive(true);
            gameOverPanel.SetActive(false);
        }
        else
        {
            SceneManager.LoadScene("Shop");
        }
    }

    public void Restart()
    {
        isPaused = false;
        Time.timeScale = 1;
        InterstitialAds.Instance.ShowInterstitial();
        StartCoroutine(restart());
        IEnumerator restart()
        {
            yield return new WaitForSeconds(0.5f);
            clickSound.Play();
            //LevelLoader.Instance.LoadLevel(1);
            SceneManager.LoadScene("Level");
        }
    }

    public void Resume()
    {
        Time.timeScale = 1;
        pauseSound.Play();
        gameMusic.UnPause();
        isPaused = false;
        pausePanel.SetActive(false);
        leaderboardPanel.SetActive(false);
        //nameWindow.SetActive(false);
    }

    public void PlayerDestroyedandGameOver()
    {
        StartCoroutine(GameisOver());
        IEnumerator GameisOver()
        {
            playerDestroySound.Play();
            gameMusic.Pause();
            pauseButton.SetActive(false);
            player.SetActive(false);
            yield return new WaitForSeconds(0.25f);
            if (Random.Range(0, 4) == 1)
            {
                rateBox.SetActive(true);
            }
            else
            {
                gameOverPanel.SetActive(true);
            }
        }
    }

    public void MainMenuPausePanel()
    {
        isPaused = false;
        InterstitialAds.Instance.ShowInterstitial();
        clickSound.Play();
        Time.timeScale = 1;
        SceneManager.LoadScene("Main Menu");
    }

    public void LeaderboardBackArrowButton()
    {
        clickSound.Play();
        leaderboardPanel.SetActive(false);
        //nameWindow.gameObject.SetActive(false);
    }

    public void UpdateScorePlayfab()
    {
        PlayfabManager.instance.SendLeaderboard(score);
    }

    public void LeaderButton()
    {
        PlayfabManager.instance.LeaderboardButton();
    }

    public void GetLeader()
    {
        PlayfabManager.instance.GetLeaderboard();
    }

    public void Later()
    {
        rateBox.SetActive(false);
        gameOverPanel.SetActive(true);
    }

    public void RateNow()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.redcubez.game.airfighter&hl=en&gl=US");
        rateBox.SetActive(false);
        gameOverPanel.SetActive(true);
    }

    public void ScoreDisplayAtLevel()
    {
        scoreDisplayAtLevel.GetComponent<Text>().text = score.ToString();

        if (score < 0)
        {
            scoreDisplayAtLevel.GetComponent<Text>().text = 0.ToString();
        }
        else
        {
            if (score < 10)
            {
                scoreDisplayAtLevel.GetComponent<Text>().text = "0" + score;
            }
            else
            {
                scoreDisplayAtLevel.GetComponent<Text>().text = score.ToString();
            }
        }
    }

    void ScoreDisplayatGameOver()
    {
        scoreDisplayatGameOver.GetComponent<Text>().text = score.ToString();

        if (score < 0)
        {
            scoreDisplayatGameOver.GetComponent<Text>().text = 0.ToString();
        }
        else
        {
            if (score < 10)
            {
                scoreDisplayatGameOver.GetComponent<Text>().text = "0" + score;
            }
            else
            {
                scoreDisplayatGameOver.GetComponent<Text>().text = score.ToString();
            }
        }
    }

    void CoinScore()
    {
        coinScore.GetComponent<Text>().text = allcoins.ToString();

        if (coins < 0)
        {
            coinScore.GetComponent<Text>().text = 0.ToString();
        }
        else
        {
            if (coins < 10)
            {
                coinScore.GetComponent<Text>().text = "0" + allcoins;
            }
            else
            {
                coinScore.GetComponent<Text>().text = allcoins.ToString();
            }
        }
    }

    void Coins()
    {
        coinScoreForLevelOnly.GetComponent<Text>().text = coins.ToString();

        if (coins < 0)
        {
            coinScoreForLevelOnly.GetComponent<Text>().text = 0.ToString();
        }
        else
        {
            if (coins < 10)
            {
                coinScoreForLevelOnly.GetComponent<Text>().text = "0" + coins;
            }
            else
            {
                coinScoreForLevelOnly.GetComponent<Text>().text = coins.ToString();
            }
        }
    }

    public void DoubleCoins()
    {
        int AdsLimit = PlayerPrefs.GetInt("AdsLimit");
        if (AdsLimit < 5)
        {
            coinsThrice = true;
            RewardedAds.Instance.RequestRewarded();
            RewardedAds.Instance.ShowRewarded();

            coinsAtButton = int.Parse(coinScoreForGameOverMultiply3.GetComponent<Text>().text);

            showAds++;
            PlayerPrefs.SetInt("AdsLimit", showAds);
            coinsThrice = false;

            Debug.Log("Bool " + AdsLimit + DateTime.Now);
            StartCoroutine(Mainmenu());
            IEnumerator Mainmenu()
            {
                yield return new WaitForSeconds(0.5f);
                clickSound.Play();
                Time.timeScale = 1;
                SceneManager.LoadScene("Main Menu");
            }
        }

        else
        {
            StartCoroutine(AdsLimiTPanel());
        }
    }

    IEnumerator AdsLimiTPanel()
    {
        AdsDailyLimitExceededPanel.SetActive(true);
        yield return new WaitForSeconds(2);
        AdsDailyLimitExceededPanel.SetActive(false);
    }
}
