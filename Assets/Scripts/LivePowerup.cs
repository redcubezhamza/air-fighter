using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivePowerup : MonoBehaviour
{
    private float speed = 1f;
    private float yRange = 5f;
    void Update()
    {
        if (Player.Instance.gameOver == false)
        {
            Move();
            DestroyOutOfBounds();
        }
    }

    public void Move()
    {
        transform.Translate(0, -speed * Time.deltaTime, 0);
    }

    public void DestroyOutOfBounds()
    {
        if (transform.position.y < -yRange)
            Destroy(gameObject);
    }
}
