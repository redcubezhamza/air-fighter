using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScene : MonoBehaviour
{
    public GameObject splashScreen;

    void Start()
    {
        SplashLoadScreen();
    }

    void SplashLoadScreen()
    {
        StartCoroutine(RunSplash());

        IEnumerator RunSplash()
        {
            splashScreen.SetActive(true);
            yield return new WaitForSeconds(4.5f);
            LevelLoader.Instance.LoadLevel(1); 
            SceneManager.LoadScene("Main Menu");
        }
    }
}
