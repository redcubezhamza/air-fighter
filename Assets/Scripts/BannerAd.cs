using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class BannerAd : MonoBehaviour
{
    public BannerView bannerAd;

    public static BannerAd Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        MobileAds.Initialize(InitializationStatus => { });

        this.RequestBanner();
    }

    public AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder().Build();
    }

    public void RequestBanner()
    {
        string adUnitId = "ca-app-pub-3940256099942544/6300978111";
        this.bannerAd = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
        this.bannerAd.LoadAd(this.CreateAdRequest());
    }
}
