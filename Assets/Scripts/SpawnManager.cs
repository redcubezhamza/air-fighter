using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] beginnerEnemyPlanePrefabs;
    public GameObject helicopterEnemyPrefab;
    public GameObject c130EnemyPrefab;
    public GameObject[] coinPrefabs;
    public GameObject[] firePrefabs;
    public GameObject livePrefab;
    public GameObject[] bulletTypesPrefabs;

    public bool parentDestroyed = false;
    public bool helicopterDestroyed = false;
    public bool helicopterDestroyedforC130 = false;
    public bool fighterDestroyed = false;
    public bool c130;
    public bool c130forc130;
    public GameObject fighterEnemyPrefab;

    private float beginnerSpawnRate = 3;
    private float helicopterSpawnRate = 15;
    private float fighterSpawnRate = 05;
    private float c130SpawnRate = 45;

    public static SpawnManager Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        c130 = false;
        c130forc130 = false;
        SpawningofBeginnerEnemyPlanes();
        SpawningofHelicopterEnemy();
        Spawningofc130Enemy();
        SpawningofCoins();
        SpawningofLive();
        SpawningofBulletTypePrefabs();

        InvokeRepeating("BeginnerSpawnRate", Random.Range(180, 300), Random.Range(180, 300));
        InvokeRepeating("HelicopterSpawnRate", 210, 240);
        InvokeRepeating("FighterSpawnRate", 210, 240);
        InvokeRepeating("C130SpawnRate", Random.Range(180, 300), Random.Range(180, 300));
    }

    void Update()
    {
        if(parentDestroyed == true)
        {
            StartCoroutine(ParentDestroy());
            IEnumerator ParentDestroy()
            {
                int randomBeginnerEnemyPlanesPrefabs = Random.Range(0, beginnerEnemyPlanePrefabs.Length);
                float randomPosX = Random.Range(-.3f, .3f);
                float randomPosY = Random.Range(4.5f, 5.5f);
                Vector3 spawnPos = new Vector3(randomPosX, randomPosY, 0);

                parentDestroyed = false;
                //yield return new WaitForSeconds(beginnerSpawnRate);
                yield return new WaitForSeconds(03);
                if (c130 == false)
                {
                    yield return new WaitUntil(() => c130 == false);
                    Instantiate(beginnerEnemyPlanePrefabs[randomBeginnerEnemyPlanesPrefabs], spawnPos, beginnerEnemyPlanePrefabs[randomBeginnerEnemyPlanesPrefabs].transform.rotation);
                }
            }           
        }

        if (helicopterDestroyed == true)
        {
            StartCoroutine(InstantiateFighter());
            IEnumerator InstantiateFighter()
            {
                float randomPos = Random.Range(-.3f, .3f);
                Vector3 spawnPos = new Vector3(randomPos, 5.5f, 0);
                helicopterDestroyed = false;
                //yield return new WaitForSeconds(fighterSpawnRate);
                yield return new WaitForSeconds(15);
                if(c130 == false)
                {
                    yield return new WaitUntil(() => c130 == false);
                    Instantiate(fighterEnemyPrefab, spawnPos, fighterEnemyPrefab.transform.rotation);
                }
            }
        }

        if (fighterDestroyed == true)
        {
            StartCoroutine(InstantiateHelicopter());
            IEnumerator InstantiateHelicopter()
            {
                float randomPos = Random.Range(-.3f, .3f);
                Vector3 spawnPos = new Vector3(randomPos, 5.5f, 0);
                fighterDestroyed = false;
                //yield return new WaitForSeconds(helicopterSpawnRate);
                yield return new WaitForSeconds(30);
                if (c130 == false)
                {
                    yield return new WaitUntil(() => c130 == false);
                    Instantiate(helicopterEnemyPrefab, spawnPos, helicopterEnemyPrefab.transform.rotation);
                }
            }
        }

        if (c130forc130 == false)
        {
            StartCoroutine(InstantiateC130());
            IEnumerator InstantiateC130()
            {
                float randomPos = Random.Range(-.3f, .3f);
                Vector3 spawnPos = new Vector3(randomPos, 5.5f, 0);
                c130forc130 = true;
                //yield return new WaitForSeconds(c130SpawnRate);
                yield return new WaitForSeconds(90);
                if (helicopterDestroyed == true)
                {
                    yield return new WaitUntil(() => helicopterDestroyed == true);
                    //c130forc130 = true;
                    Instantiate(c130EnemyPrefab, spawnPos, c130EnemyPrefab.transform.rotation);
                }
            }
        }
    }

    float BeginnerSpawnRate()
    {
        beginnerSpawnRate /= 2;
        return beginnerSpawnRate;
    }

    float HelicopterSpawnRate()
    {
        helicopterSpawnRate /= 2;
        return helicopterSpawnRate;
    }

    float FighterSpawnRate()
    {
        fighterSpawnRate /= 2;
        return fighterSpawnRate;
    }

    float C130SpawnRate()
    {
        c130SpawnRate /= 2;
        return c130SpawnRate;
    }

    void SpawningofBulletTypePrefabs()
    {
        InvokeRepeating("SpawnBulletTypePrefabs", 60, 60);
        //InvokeRepeating("SpawnBulletTypePrefabs", 5, 5);
    }

    void SpawnBulletTypePrefabs()
    {
        if (Player.Instance.gameOver == false)
        {
            int bulletTypesPrefab = Random.Range(0, bulletTypesPrefabs.Length);
            float randomPosX = Random.Range(-1.5f, 1.5f);
            float randomPosY = Random.Range(4.5f, 5.5f);
            Vector3 spawnPos = new Vector3(randomPosX, randomPosY, 0);
            GameObject GO = Instantiate(bulletTypesPrefabs[bulletTypesPrefab], spawnPos, bulletTypesPrefabs[bulletTypesPrefab].transform.rotation);
        }
    }

    void SpawningofLive()
    {
        InvokeRepeating("SpawnLive", 120, 120);
        //InvokeRepeating("SpawnLive", 10, 10);
    }

    void SpawnLive()
    {
        if (Player.Instance.gameOver == false)
        {
            float randomPos = Random.Range(-2f, 2f);
            Vector3 spawnPos = new Vector3(randomPos, 5.5f, 0);
            GameObject GO = Instantiate(livePrefab, spawnPos, livePrefab.transform.rotation);
        }
    }

    void SpawningofBeginnerEnemyPlanes()
    {
        Invoke("SpawnBeginnerEnemyPlanePrefabs", 3);
    }

    void SpawnBeginnerEnemyPlanePrefabs()
    {
        if (Player.Instance.gameOver == false)
        {
            int randomBeginnerEnemyPlanesPrefabs = Random.Range(0, beginnerEnemyPlanePrefabs.Length);
            float randomPosX = Random.Range(-.3f, .3f);
            float randomPosY = Random.Range(4.5f, 5.5f);
            Vector3 spawnPos = new Vector3(randomPosX, randomPosY, 0);
            Instantiate(beginnerEnemyPlanePrefabs[randomBeginnerEnemyPlanesPrefabs], spawnPos, beginnerEnemyPlanePrefabs[randomBeginnerEnemyPlanesPrefabs].transform.rotation);
        }
    }

    void SpawningofHelicopterEnemy()
    {
        Invoke("SpawnHelicopterEnemyPrefab", 30);
    }

    void SpawnHelicopterEnemyPrefab()
    {
        if (Player.Instance.gameOver == false)
        {
            float randomPos = Random.Range(-.3f, .3f);
            Vector3 spawnPos = new Vector3(randomPos, 5.5f, 0);
            Instantiate(helicopterEnemyPrefab, spawnPos, helicopterEnemyPrefab.transform.rotation);
        }
    }

    void Spawningofc130Enemy()
    {
        Invoke("Spawnc130EnemyPrefab", 90);
    }

    void Spawnc130EnemyPrefab()
    {
        float randomPos = Random.Range(-.3f, .3f);
        Vector3 spawnPos = new Vector3(randomPos, 5.5f, 0);

        if (Player.Instance.gameOver == false)
        {
            c130 = true;
            c130forc130 = true;
            Instantiate(c130EnemyPrefab, spawnPos, c130EnemyPrefab.transform.rotation);
        }
    }

    void SpawningofCoins()
    {
        InvokeRepeating("SpawnCoins", Random.Range(3, 6), Random.Range(20, 65));
    }

    void SpawnCoins()
    {
        if (Player.Instance.gameOver == false)
        {
            int randomCoins = Random.Range(0, coinPrefabs.Length);
            float randomPos = Random.Range(-1.8f, 1.8f);
            Vector3 spawnPos = new Vector3(randomPos, 5f, 0);
            Instantiate(coinPrefabs[randomCoins], spawnPos, coinPrefabs[randomCoins].transform.rotation);
        }
    }
}
