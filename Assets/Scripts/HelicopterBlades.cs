using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterBlades : MonoBehaviour
{
    private float speed = 1000f;

    void Update()
    {
        transform.Rotate(new Vector3(0, 0, -speed * Time.deltaTime));
    }
}
