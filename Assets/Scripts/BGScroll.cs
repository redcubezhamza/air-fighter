using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGScroll : MonoBehaviour
{
    [SerializeField] float scrollSpeed = .03f;
    public MeshRenderer mr;
    public SpriteRenderer sr;
    [SerializeField] float yScroll;

    [SerializeField] private MapsSkinManager mapsSkinManager;

    private void Start()
    {
        //GetComponent<SpriteRenderer>().sprite = mapsSkinManager.GetSelectedSkin().sprite;

        GetComponent<MeshRenderer>().material = mapsSkinManager.GetSelectedSkin().material;
    }

    void Update()
    {
        if (Player.Instance.gameOver == false)
            Scroll();
    }

    void Scroll()
    {
        Vector2 offset = mr.material.mainTextureOffset;
        offset = offset + new Vector2(0, scrollSpeed * Time.deltaTime);
        mr.material.mainTextureOffset = offset;
    }
}
