using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMainMenu : MonoBehaviour
{
    public static BGMainMenu instance;
    public float bgSpeed, speedDivider;
    public Renderer bgRenderer;



    [SerializeField] private MapsSkinManager mapsSkinManager;

    private void Start()
    {
        instance = this;

        GetSelectedSkinMaterial();
    }

    void Update()
    {
        //GetComponent<SpriteRenderer>().sprite = mapsSkinManager.GetSelectedSkin().sprite;
        



        bgRenderer.material.mainTextureOffset += new Vector2(0f, bgSpeed / speedDivider * Time.deltaTime);
    }

    void GetSelectedSkinMaterial()
    {
        GetComponent<MeshRenderer>().material = mapsSkinManager.GetSelectedSkin().material;
    }
}
