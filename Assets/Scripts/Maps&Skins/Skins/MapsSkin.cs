using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MapsSkin
{
    public int cost;
    public Sprite sprite;

    public Material material;
}
