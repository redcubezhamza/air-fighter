using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapsSkinShopItem : MonoBehaviour
{
    [SerializeField] private MapsSkinManager mapsSkinManager;
    [SerializeField] private int mapsSkinIndex;
    [SerializeField] private Button buyButton;
    [SerializeField] private Text costText;
    private MapsSkin mapsSkin;

    private float coinsValueConverter;

    void Start()
    {
        mapsSkin = mapsSkinManager.mapsSkins[mapsSkinIndex];
        //GetComponent<Image>().sprite = mapsSkin.sprite;

        GetComponent<Image>().material = mapsSkin.material;

        if (mapsSkinManager.IsUnlocked(mapsSkinIndex))
        {
            buyButton.gameObject.SetActive(false);
        }
        else
        {
            buyButton.gameObject.SetActive(true);
            //costText.text = mapsSkin.cost.ToString();
            CashConverter();
            if (mapsSkin.cost >= 1000)
            {
                costText.text = coinsValueConverter.ToString("F0") + "K";
            }

            if (mapsSkin.cost >= 1000000)
            {
                costText.text = coinsValueConverter.ToString("F0") + "M";
            }

            if (mapsSkin.cost >= 1000000000)
            {
                costText.text = coinsValueConverter.ToString("F5") + "B";
            }

            if (mapsSkin.cost < 1000)
            {
                costText.text = mapsSkin.cost.ToString();
            }
        }
    }

    public void CashConverter()
    {
        if (mapsSkin.cost >= 1000)
        {
            coinsValueConverter = mapsSkin.cost / 1000;
        }

        if (mapsSkin.cost >= 1000000)
        {
            coinsValueConverter = mapsSkin.cost / 1000000;
        }

        if (mapsSkin.cost >= 1000000000)
        {
            coinsValueConverter = mapsSkin.cost / 1000000000;
        }
    }

    public void OnSkinPressed()
    {
        if (mapsSkinManager.IsUnlocked(mapsSkinIndex))
        {
            mapsSkinManager.SelectSkin(mapsSkinIndex);
        }
    }

    public void OnBuyButtonPressed()
    {
        int coins = PlayerPrefs.GetInt("Coins", 0);

        if (coins >= mapsSkin.cost && !mapsSkinManager.IsUnlocked(mapsSkinIndex))
        {
            PlayerPrefs.SetInt("Coins", coins - mapsSkin.cost);
            mapsSkinManager.Unlock(mapsSkinIndex);
            buyButton.gameObject.SetActive(false);
            mapsSkinManager.SelectSkin(mapsSkinIndex);
        }
        else
        {
            Debug.Log("Not enough coins!");
        }
    }
}
