using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MapsShopController : MonoBehaviour
{
    [SerializeField] private Image mapsSelectedSkin;
    [SerializeField] private Text coinsText;
    [SerializeField] private MapsSkinManager mapsSkinManager;

    void Update()
    {
        //coinsText.text = "Coins: " + PlayerPrefs.GetInt("Coins");
        coinsText.text = PlayerPrefs.GetInt("Coins").ToString();
        //mapsSelectedSkin.sprite = mapsSkinManager.GetSelectedSkin().sprite;

        mapsSelectedSkin.material = mapsSkinManager.GetSelectedSkin().material;
    }

    public void LoadMenu() => SceneManager.LoadScene("Level");
}
