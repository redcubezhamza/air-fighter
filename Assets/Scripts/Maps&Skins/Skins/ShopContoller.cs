using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShopContoller : MonoBehaviour
{
    [SerializeField] private Image selectedSkin;
    [SerializeField] private Text coinsText;
    [SerializeField] private SkinManager skinManager;

    void Update()
    {
        //coinsText.text = "Coins: " + PlayerPrefs.GetInt("Coins");
        coinsText.text = PlayerPrefs.GetInt("Coins").ToString();
        selectedSkin.sprite = skinManager.GetSelectedSkin().sprite;
    }

    public void LoadMenu() => SceneManager.LoadScene("Level");
}
