using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MapsSkinManager", menuName = "Maps Skin Manager")]

public class MapsSkinManager : ScriptableObject
{
    [SerializeField] public MapsSkin[] mapsSkins;
    private const string Prefix = "MapsSkin_";
    private const string SelectedSkin = "SelectedMapsSkin";

    public void SelectSkin(int mapsSkinIndex) => PlayerPrefs.SetInt(SelectedSkin, mapsSkinIndex);

    public MapsSkin GetSelectedSkin()
    {
        int mapsSkinIndex = PlayerPrefs.GetInt(SelectedSkin, 0);
        if (mapsSkinIndex >= 0 && mapsSkinIndex < mapsSkins.Length)
        {
            return mapsSkins[mapsSkinIndex];
        }
        else
        {
            return null;
        }
    }

    public void Unlock(int mapsSkinIndex) => PlayerPrefs.SetInt(Prefix + mapsSkinIndex, 1);

    public bool IsUnlocked(int mapsSkinIndex) => PlayerPrefs.GetInt(Prefix + mapsSkinIndex, 0) == 1;
}
