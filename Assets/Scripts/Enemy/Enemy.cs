using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public GameObject enemyBullet;
    public Transform attackPointMid;
    public Transform attackPointRight;
    public Transform attackPointLeft;
    public Transform attackPoint01;
    public Transform attackPoint02;
    public Transform attackPoint03;
    public Transform attackPoint04;
    public Transform attackPoint05;
    public Transform attackPoint06;
    public Transform attackPoint07;
    public Transform attackPoint08;
    public GameObject lookAtAP01;

    public float attackTimer = 0.35f;
    public float currentAttackTimer;
    public bool canAttack;

    private float speed = 1f;
    private float yRange = 5f;
    private float xRange = 3f;

    public GameObject c130FrontBullet;
    public GameObject c130RightBullet;
    public GameObject c130LeftBullet;

    public IEnumerator FireBurstFighterEnemy(GameObject bulletPrefab, int burstSize, float rateOfFire)
    {
        if (Player.Instance.gameOver == false)
        {
            float bulletDelay = 60 / rateOfFire;
            for (int i = 0; i < burstSize; i++)
            {
                Instantiate(enemyBullet, attackPointMid.position, Quaternion.identity);
                Instantiate(enemyBullet, attackPoint01.position, Quaternion.identity);
                Instantiate(enemyBullet, attackPoint02.position, Quaternion.identity);
                Instantiate(enemyBullet, attackPoint03.position, Quaternion.identity);
                Instantiate(enemyBullet, attackPoint04.position, Quaternion.identity);

                yield return new WaitForSeconds(bulletDelay);
            }
        }          
    }

    public IEnumerator FireBurstC130EnemyFrontBullets(GameObject bulletPrefab, int burstSize, float rateOfFire)
    {
        if (Player.Instance.gameOver == false)
        {
            float bulletDelay = 60 / rateOfFire;
            for (int i = 0; i < burstSize; i++)
            {
                Instantiate(c130FrontBullet, attackPointMid.position, Quaternion.identity);
                Instantiate(c130FrontBullet, attackPoint01.position, Quaternion.identity);
                Instantiate(c130FrontBullet, attackPoint02.position, Quaternion.identity);
                Instantiate(c130FrontBullet, attackPoint03.position, Quaternion.identity);
                Instantiate(c130FrontBullet, attackPoint04.position, Quaternion.identity);

                yield return new WaitForSeconds(bulletDelay);
            }
        }        
    }

    public void Fire()
    {
        FireBullets1.Instance.Fire();
        FireBullets1.Instance.Fire02();
    }

    public virtual void Move()
    {
        transform.Translate(0, speed * Time.deltaTime, 0);
    }

    public void EnemyDestroyOutOfBounds()
    {
        if (transform.position.y < -yRange)
            Destroy(gameObject); 

        if (transform.position.x < -xRange)
            Destroy(gameObject);

        if (transform.position.x > xRange)
            Destroy(gameObject);
    }
}
