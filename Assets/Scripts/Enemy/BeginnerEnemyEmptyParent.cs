using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeginnerEnemyEmptyParent : MonoBehaviour
{
    void Update()
    {
        DestroyParent();
    }

    public void DestroyParent()
    {
        if (this.gameObject.transform.childCount == 0)
        {
            SpawnManager.Instance.parentDestroyed = true;
            Destroy(this.gameObject);            
        }
    }   
}
