using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BeginnerEnemyPlanes6 : Enemy
{
    public GameObject beginnerEnemyPlaneBulletHealth;
    public GameObject beginnerEnemyPlaneBullet = null;

    private GameObject player;

    private int Speed = 1;

    public GameObject beginnerCoinsPrefab;
    public GameObject blastPrefab;

    void Start()
    {
        player = GameObject.Find("Player");
        StartCoroutine(BeginnerEnemyAttackOnceOnly());
        InvokeRepeating("IncreaseSpeed", Random.Range(180, 300), Random.Range(180, 300));
    }

    void Update()
    {
        if (Player.Instance.gameOver == false)
        {         
            Move();
            EnemyDestroyOutOfBounds();
        }
    }

    int IncreaseSpeed()
    {
        Speed *= 2;
        return Speed;
    }

    public override void Move()
    {
        transform.Translate(0, Speed * Time.deltaTime, 0);
    }

    public void AttackOnBeginnerEnemyPlanes(GameObject beginnerEnemyPlane, float damage)
    {
        beginnerEnemyPlaneBulletHealth.GetComponent<Slider>().value -= damage;
    }

    public void PlayerAttackOnBeginnerEnemyPlanes()
    {
        //AttackOnBeginnerEnemyPlanes(beginnerEnemyPlaneBullet, 100);
        AttackOnBeginnerEnemyPlanes(beginnerEnemyPlaneBullet, 10);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "PlayerBullet")
        {
            if (beginnerEnemyPlaneBulletHealth.GetComponent<Slider>().value > 0)
            {
                PlayerAttackOnBeginnerEnemyPlanes();
                GameManager.Instance.hitOnEnemySound.Play();
                Destroy(col.gameObject);                
            }
            else
            {
                Instantiate(blastPrefab, gameObject.transform.position, gameObject.transform.rotation);
                Destroy(gameObject);
                Instantiate(beginnerCoinsPrefab, transform.position, Quaternion.identity);
                Player.Instance.beginnerEnemySound.Play();
                GameManager.Instance.score += 10;
                //GameManager.Instance.coins += 1;
            }
        }

        if (col.tag == "Player")
        {
            Instantiate(blastPrefab, gameObject.transform.position, gameObject.transform.rotation);
            Destroy(gameObject);
            Instantiate(beginnerCoinsPrefab, transform.position, Quaternion.identity);
            Player.Instance.beginnerEnemySound.Play();
            GameManager.Instance.score += 10;
            //GameManager.Instance.coins += 1;
        }
    }

    IEnumerator BeginnerEnemyAttackOnceOnly()
    {
        yield return new WaitForSeconds(4.0f);
        Instantiate(enemyBullet, attackPointMid.position, Quaternion.identity);
    }
}
