using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FighterEnemy : Enemy
{
    public GameObject fighterEnemyBulletHealth;
    public GameObject fighterEnemyBullet = null;

    private float Speed = 2;

    public GameObject fighterCoinsPrefab;
    public GameObject blastPrefab;

    void Start()
    {
        InvokeRepeating("FighterBurst", 2, 2);
        InvokeRepeating("IncreaseSpeed", Random.Range(180, 300), Random.Range(180, 300));
    }

    void Update()
    {
        if (Player.Instance.gameOver == false)
        {
            Move();
            EnemyDestroyOutOfBounds();
        }
    }

    float IncreaseSpeed()
    {
        Speed *= 2;
        return Speed;
    }

    public void FighterBurst()
    {
        StartCoroutine(FireBurstFighterEnemy(enemyBullet, 1, 600));
    }

    public void AttackOnFighterEnemy(GameObject fighterEnemy, float damage)
    {
        fighterEnemyBulletHealth.GetComponent<Slider>().value -= damage;
    }

    public void PlayerAttackOnFighterEnemy()
    {
        //AttackOnFighterEnemy(fighterEnemyBullet, 2f);
        AttackOnFighterEnemy(fighterEnemyBullet, 1f);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "PlayerBullet")
        {
            if (fighterEnemyBulletHealth.GetComponent<Slider>().value > 0)
            {
                PlayerAttackOnFighterEnemy();
                GameManager.Instance.hitOnEnemySound.Play();
                Destroy(col.gameObject);
            }
            else
            {
                Instantiate(blastPrefab, gameObject.transform.position, gameObject.transform.rotation);
                SpawnManager.Instance.fighterDestroyed = true;
                Destroy(gameObject);
                Instantiate(fighterCoinsPrefab, transform.position, Quaternion.identity);
                Player.Instance.fighterSound.Play();
                GameManager.Instance.score += 500;
            }
        }

        if (col.tag == "Player")
        {
            Instantiate(blastPrefab, gameObject.transform.position, gameObject.transform.rotation);
            SpawnManager.Instance.fighterDestroyed = true;
            Destroy(gameObject);
            Instantiate(fighterCoinsPrefab, transform.position, Quaternion.identity);
            Player.Instance.fighterSound.Play();
            GameManager.Instance.score += 500;
        }

        if (col.tag == "FighterSensor")
        {
            Instantiate(blastPrefab, gameObject.transform.position, gameObject.transform.rotation);
            SpawnManager.Instance.fighterDestroyed = true;
            Destroy(gameObject);
            Instantiate(fighterCoinsPrefab, transform.position, Quaternion.identity);
        }
    }

    public override void Move()
    {
        transform.Translate(0, Speed * Time.deltaTime, 0);
    }
}
