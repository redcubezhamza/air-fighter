using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class c130Enemy : Enemy
{
    public GameObject c130EnemyBulletHealth;
    public GameObject c130EnemyBullet = null;

    [SerializeField] Transform[] positions;
    Transform nextPos;
    int nextPosIndex;

    private float Speed = 1.5f;

    public GameObject c130CoinsPrefab;
    public GameObject blastPrefab;

    void Start()
    {
        nextPos = positions[0];

        InvokeRepeating("C130BurstSequence", 2, 4);
        InvokeRepeating("IncreaseSpeed", Random.Range(180, 300), Random.Range(180, 300));
    }

    void Update()
    {
        if (Player.Instance.gameOver == false)
        {
            Move();
            EnemyDestroyOutOfBounds();
        }
    }

    public void C130BurstSequence()
    {
        StartCoroutine(C130EnemyPlane());
        IEnumerator C130EnemyPlane()
        {
            StartCoroutine(FireBurstC130EnemyFrontBullets(c130FrontBullet, 1, 300));
            yield return new WaitForSeconds(02);
            Fire();
        }
    }
    float IncreaseSpeed()
    {
        Speed *= 2;
        return Speed;
    }

    public override void Move()
    {
        if (transform.position == nextPos.position)
        {
            nextPosIndex++;
            if (nextPosIndex >= positions.Length)
            {
                nextPosIndex = 0;
            }
            nextPos = positions[nextPosIndex];
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, nextPos.position, Speed * Time.deltaTime);
        }
    }

    public void AttackOnc130Enemy(GameObject c130Enemy, float damage)
    {
        c130EnemyBulletHealth.GetComponent<Slider>().value -= damage;
    }

    public void PlayerAttackOnc130Enemy()
    {
        AttackOnc130Enemy(c130EnemyBullet, 0.7f);
        //AttackOnc130Enemy(c130EnemyBullet, 0.1f);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "PlayerBullet")
        {
            if (c130EnemyBulletHealth.GetComponent<Slider>().value > 0)
            {
                PlayerAttackOnc130Enemy();
                GameManager.Instance.hitOnEnemySound.Play();
                Destroy(col.gameObject);
            }
            else
            {
                Instantiate(blastPrefab, gameObject.transform.position, gameObject.transform.rotation);
                SpawnManager.Instance.parentDestroyed = true;
                SpawnManager.Instance.c130 = false;
                SpawnManager.Instance.c130forc130 = false;
                Destroy(gameObject);
                Instantiate(c130CoinsPrefab, transform.position, Quaternion.identity);
                Player.Instance.c130Sound.Play();
                GameManager.Instance.score += 1000;
            }
        }

        if (col.tag == "Player")
        {
            Instantiate(blastPrefab, gameObject.transform.position, gameObject.transform.rotation);
            SpawnManager.Instance.parentDestroyed = true;
            SpawnManager.Instance.c130 = false;
            SpawnManager.Instance.c130forc130 = false;
            Destroy(gameObject);
            Instantiate(c130CoinsPrefab, transform.position, Quaternion.identity);
            Player.Instance.c130Sound.Play();
            GameManager.Instance.score += 1000;
        }
    }
}
