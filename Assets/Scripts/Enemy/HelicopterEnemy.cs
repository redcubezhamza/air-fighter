using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelicopterEnemy : Enemy
{
    public GameObject helicopterEnemyBulletHealth;
    public GameObject helicopterEnemyBullet = null;

    public GameObject fighterPrefab;

    [SerializeField] Transform[] positions;
    Transform nextPos;
    int nextPosIndex;

    [SerializeField] GameObject burst;
    [SerializeField] int spawnCounter = 0;
    private float Speed = 2.25f;

    public Transform burstCenterPoint;

    public GameObject heliCoinsPrefab;
    public GameObject blastPrefab;
    
    public static HelicopterEnemy Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        nextPos = positions[0];
        InvokeRepeating("SpawnBurst", 5, 5);
        InvokeRepeating("IncreaseSpeed", Random.Range(180,300), Random.Range(180, 300));
    }

    void Update()
    {
        if (Player.Instance.gameOver == false)
        {
            Move();
            EnemyDestroyOutOfBounds();
        }
    }

    int SpawnBurst()
    {
        if(Player.Instance.gameOver == false)
        {
            GameObject GO = Instantiate(burst, burstCenterPoint.position, Quaternion.identity);
            Destroy(GO, 5);
            spawnCounter++;            
        }
        return spawnCounter;
    }

    float IncreaseSpeed()
    {
        Speed *= 2;
        return Speed;
    }

    public override void Move()
    {
        if(transform.position == nextPos.position)
        {
            nextPosIndex++;
            if(nextPosIndex >= positions.Length)
            {
                nextPosIndex = 0;
            }
            nextPos = positions[nextPosIndex];
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, nextPos.position, Speed * Time.deltaTime);
        }
    }

    public void AttackOnHelicopterEnemy(GameObject helicopterEnemy, float damage)
    {
        helicopterEnemyBulletHealth.GetComponent<Slider>().value -= damage;
    }

    public void PlayerAttackOnHelicopterEnemy()
    {
        //AttackOnHelicopterEnemy(helicopterEnemyBullet, 0.35f);
        AttackOnHelicopterEnemy(helicopterEnemyBullet, 1.5f);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "PlayerBullet")
        {
            if (helicopterEnemyBulletHealth.GetComponent<Slider>().value > 0)
            {
                PlayerAttackOnHelicopterEnemy();
                GameManager.Instance.hitOnEnemySound.Play();
                Destroy(col.gameObject);
            }
            else
            {
                Instantiate(blastPrefab, gameObject.transform.position, gameObject.transform.rotation);
                SpawnManager.Instance.helicopterDestroyed = true;
                SpawnManager.Instance.helicopterDestroyedforC130 = true;
                Destroy(gameObject);
                Instantiate(heliCoinsPrefab, transform.position, Quaternion.identity);
                Player.Instance.helicopterSound.Play();
                GameManager.Instance.score += 100;
            }
        }

        if (col.tag == "Player")
        {
            Instantiate(blastPrefab, gameObject.transform.position, gameObject.transform.rotation);
            SpawnManager.Instance.helicopterDestroyed = true;
            SpawnManager.Instance.helicopterDestroyedforC130 = true;
            Destroy(gameObject);
            Instantiate(heliCoinsPrefab, transform.position, Quaternion.identity);
            Player.Instance.helicopterSound.Play();
            GameManager.Instance.score += 100;
        }
    }
}
