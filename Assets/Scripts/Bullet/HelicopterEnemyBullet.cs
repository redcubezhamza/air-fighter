using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelicopterEnemyBullet : Bullet
{
    [SerializeField] Vector3 startRotation;
    [SerializeField] float Speed = 0.5f;

    void Update()
    {
        if (Player.Instance.gameOver == false)
        {
            Move();
            BulletDestroyOutOfBounds();
        }
    }

    public override void Move()
    {
        transform.Translate(startRotation * Time.deltaTime * Speed);
    }
}
