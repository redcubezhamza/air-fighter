using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBullet : Bullet
{
    
    void Start()
    {
        //Invoke("DeactivateGameObject", deactivateTimer);
    }

    void Update()
    {
        if(Player.Instance.gameOver == false)
        {
            Move();
            BulletDestroyOutOfBounds();
        }        
    }

    public override void BulletDestroyOutOfBounds()
    {
        if (transform.position.y > yRange)
            Destroy(gameObject);
    }

    public override void Move()
    {
        Vector3 temp = transform.position;
        temp.y += 6 * Time.deltaTime;
        transform.position = temp;
    }
}
