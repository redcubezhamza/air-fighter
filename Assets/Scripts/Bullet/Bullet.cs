using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 5f;
    public float deactivateTimer = 2f;

    public float yRange = 5f;

    public virtual void Move()
    {
        Vector3 temp = transform.position;
        temp.y += speed * Time.deltaTime;
        transform.position = temp;
    }

    public void DeactivateGameObject()
    {
        Destroy(gameObject);
    }

    public virtual void BulletDestroyOutOfBounds()
    {
        if (transform.position.y < -yRange)
            Destroy(gameObject);
    }
}
