using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class c130LeftBullet : Bullet
{
    void Update()
    {
        if (Player.Instance.gameOver == false)
        {
            Move();
            BulletDestroyOutOfBounds();
        }
    }

    public override void Move()
    {
        Vector3 temp = transform.position;
        temp.y -= speed * Time.deltaTime;
        temp.x -= speed * Time.deltaTime;
        transform.position = temp;
    }
}
