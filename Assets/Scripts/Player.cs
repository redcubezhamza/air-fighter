using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private SkinManager skinManager;

    public Rigidbody2D rb;

    private float xRange = 2f;
    private float upperYRange = 3.5f;
    private float lowerYRange = 3.5f;
    private float zRange = 0f;

    private Touch touch;
    private float sensitivity = 0.005f;
   
    [SerializeField] private GameObject fireBullets;

    [SerializeField] private Transform attackPoint01;
    [SerializeField] private Transform attackPoint02;
    [SerializeField] private Transform attackPoint03;
    [SerializeField] private Transform attackPoint04;
    [SerializeField] private Transform attackPoint05;
    [SerializeField] private Transform attackPoint06;
    [SerializeField] private Transform attackPoint07;
    [SerializeField] private Transform attackPoint08;
    [SerializeField] private Transform attackPoint09;
    [SerializeField] private Transform attackPoint10;
    [SerializeField] private Transform attackPoint11;
    [SerializeField] private Transform attackPoint12;
    [SerializeField] private Transform attackPoint13;

    public bool gameOver = false;
    public bool isRespawning = false;

    public GameObject playerHealth;
    public GameObject player = null;

    public GameObject liveScore;
    public int liveScoreValue;
    public GameObject heart;
    public GameObject heart1;
    public GameObject heart2;

    public GameObject[] firePrefabs;

    public AudioSource coinCollectSound;
    public AudioSource beginnerEnemySound;
    public AudioSource helicopterSound;
    public AudioSource fighterSound;
    public AudioSource c130Sound;
    public AudioSource playerFiringSound;

    public ParticleSystem playerParticles;

    GameObject GO1;
    GameObject GO2;
    GameObject GO3;
    GameObject GO4;
    GameObject GO5;
    GameObject GO6;

    public Sprite fire;
    public Sprite arrow;
    public Sprite diamond;

    SpriteRenderer rend;
    Color color;

    public static Player Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        GetComponent<SpriteRenderer>().sprite = skinManager.GetSelectedSkin().sprite;

        rend = GetComponent<SpriteRenderer>();
        color = rend.color;

        InvokeRepeating("PlayerBurst01", 1, .2f);
        InvokeRepeating("InstantiateFire01", Random.Range(15, 20), Random.Range(15, 20));
    }

    void Update()
    {
        TouchInput();
        XAxisRange();
        YAxisRange();
        ZAxisRange();
        Lives();
        Heart();
    }

    IEnumerator RespawnOnSingleLive()
    {
        isRespawning = true;
        color.a = 0.2f;
        rend.color = color;
        yield return new WaitForSeconds(.25f);
        color.a = 1f;
        rend.color = color;
        yield return new WaitForSeconds(.25f);
        color.a = 0.2f;
        rend.color = color;
        yield return new WaitForSeconds(.25f);
        color.a = 1f;
        rend.color = color;
        yield return new WaitForSeconds(.25f);
        color.a = 0.2f;
        rend.color = color;
        yield return new WaitForSeconds(.25f);
        color.a = 1f;
        rend.color = color;
        yield return new WaitForSeconds(.25f);
        color.a = 0.2f;
        rend.color = color;
        yield return new WaitForSeconds(.25f);
        color.a = 1f;
        rend.color = color;
        yield return new WaitForSeconds(.25f);
        color.a = 0.2f;
        rend.color = color;
        yield return new WaitForSeconds(.25f);
        color.a = 1f;
        rend.color = color;
        yield return new WaitForSeconds(.25f);
        color.a = 0.2f;
        rend.color = color;
        yield return new WaitForSeconds(.25f);
        color.a = 1f;
        rend.color = color;
        isRespawning = false;
    }

    public void Heart()
    {
        if (liveScoreValue == 1)
        {
            heart.SetActive(true);
            heart1.SetActive(false);
            heart2.SetActive(false);
        }
        if (liveScoreValue == 2)
        {
            heart.SetActive(true);
            heart1.SetActive(true);
            heart2.SetActive(false);
        }
        if (liveScoreValue == 3)
        {
            heart.SetActive(true);
            heart1.SetActive(true);
            heart2.SetActive(true);
        }
        if (liveScoreValue > 3)
        {
            heart.SetActive(true);
            heart1.SetActive(true);
            heart2.SetActive(true);
        }
        if (liveScoreValue < 1)
        {
            heart.SetActive(false);
            heart1.SetActive(false);
            heart2.SetActive(false);
        }
    }

    public void Lives()
    {
        if (liveScoreValue < 1)
        {
            liveScore.GetComponent<Text>().text = "00";
            gameOver = true;
            GameManager.Instance.PlayerDestroyedandGameOver();
            playerHealth.GetComponent<Slider>().value = 0;
            CancelInvokes();
        }
        else if (liveScoreValue > 3)
        {
            liveScoreValue = 03;
            liveScore.GetComponent<Text>().text = "03";
        }
        else
        {
            liveScore.GetComponent<Text>().text = "0" + liveScoreValue;
        }
    }

    public void AttackOnPlayer(GameObject player, float damage)
    {
        playerHealth.GetComponent<Slider>().value -= damage;
    }

    public void EnemyAttackByBeginnerEnemyPlane()
    {
        AttackOnPlayer(player, 02);
        //AttackOnPlayer(player, 70);
    }

    public void EnemyAttackByHelicopterEnemy()
    {
        AttackOnPlayer(player, 20);
    }

    public void EnemyAttackByFighterEnemy()
    {
        AttackOnPlayer(player, 20);
    }

    public void EnemyAttackByc130Enemy()
    {
        //AttackOnPlayer(player, 50f);
        AttackOnPlayer(player, 10f);
    }

    void TouchInput()
    {
        if(GameManager.Instance.isPaused == false)
        {
            if (Input.touchCount > 0)
            {
                touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Moved)
                {
                    transform.position = new Vector3(
                        transform.position.x + touch.deltaPosition.x * sensitivity,
                        transform.position.y + touch.deltaPosition.y * sensitivity,
                        transform.position.z);
                }
            }
        }
    }

    void XAxisRange()
    {
        if (transform.position.x < -xRange)
        {
            transform.position = new Vector3(-xRange, transform.position.y, transform.position.z);
        }
        if (transform.position.x > xRange)
        {
            transform.position = new Vector3(xRange, transform.position.y, transform.position.z);
        }
    }

    void YAxisRange()
    {
        if (transform.position.y < -lowerYRange)
        {
            transform.position = new Vector3(transform.position.x, -lowerYRange, transform.position.z);
        }
        if (transform.position.y > upperYRange)
        {
            transform.position = new Vector3(transform.position.x, upperYRange, transform.position.z);
        }
    }

    void ZAxisRange()
    {
        if (transform.position.z < -zRange)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -zRange);
        }
        if (transform.position.z > zRange)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, zRange);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {      
        if (col.tag == "Coin")
        {
            Destroy(col.gameObject);
            GameManager.Instance.coins++;
            coinCollectSound.Play();
        }

        if (col.tag == "BeginnerEnemyPlanesBullet")
        {
            if (playerHealth.GetComponent<Slider>().value > 0)
            {
                if(isRespawning == false)
                {
                    EnemyAttackByBeginnerEnemyPlane();
                    Destroy(col.gameObject);
                }                
            }
            else
            {
                liveScoreValue--;
                playerHealth.GetComponent<Slider>().value = 100;
                Destroy(GO1);
                Destroy(GO2);
                Destroy(GO3);
                Destroy(GO4);
                Destroy(GO5);
                Destroy(GO6);
                StartCoroutine(RespawnOnSingleLive());
                CancelInvokes();
            }
        }

        if (col.tag == "HelicopterEnemysBullet")
        {
            if (playerHealth.GetComponent<Slider>().value > 0)
            {
                if (isRespawning == false)
                {
                    EnemyAttackByHelicopterEnemy();
                    Destroy(col.gameObject);
                }
            }
            else
            {
                liveScoreValue--;
                playerHealth.GetComponent<Slider>().value = 100;
                Destroy(GO1);
                Destroy(GO2);
                Destroy(GO3);
                Destroy(GO4);
                Destroy(GO5);
                Destroy(GO6);
                StartCoroutine(RespawnOnSingleLive());
                CancelInvokes();
            }
        }

        if (col.tag == "FighterEnemysBullet")
        {
            if (playerHealth.GetComponent<Slider>().value > 0)
            {
                if (isRespawning == false)
                {
                    EnemyAttackByFighterEnemy();
                    Destroy(col.gameObject);
                }                
            }
            else
            {
                liveScoreValue--;
                playerHealth.GetComponent<Slider>().value = 100;
                Destroy(GO1);
                Destroy(GO2);
                Destroy(GO3);
                Destroy(GO4);
                Destroy(GO5);
                Destroy(GO6);
                StartCoroutine(RespawnOnSingleLive());
                CancelInvokes();
            }
        }

        if (col.tag == "c130EnemysBullet")
        {
            if (playerHealth.GetComponent<Slider>().value > 0)
            {
                if (isRespawning == false)
                {
                    EnemyAttackByc130Enemy();
                    Destroy(col.gameObject);
                }                
            }
            else
            {
                liveScoreValue--;
                playerHealth.GetComponent<Slider>().value = 100;
                Destroy(GO1);
                Destroy(GO2);
                Destroy(GO3);
                Destroy(GO4);
                Destroy(GO5);
                Destroy(GO6);
                StartCoroutine(RespawnOnSingleLive());
                CancelInvokes();
            }
        }

        if (col.tag == "BeginnerEnemyPlane")
        {
            if (isRespawning == false)
            {
                AttackOnPlayer(player, 02);
                //AttackOnPlayer(player, 100);
            }            
        }

        if (col.tag == "HelicopterEnemy")
        {
            if (isRespawning == false)
            {
                AttackOnPlayer(player, 20);
            }
        }

        if (col.tag == "FighterEnemy")
        {
            if (isRespawning == false)
            {
                AttackOnPlayer(player, 20);
            }
        }

        if (col.tag == "C130Enemy")
        {
            liveScoreValue--;
            playerHealth.GetComponent<Slider>().value = 100;
            Destroy(GO1);
            Destroy(GO2);
            Destroy(GO3);
            Destroy(GO4);
            Destroy(GO5);
            Destroy(GO6);
            StartCoroutine(RespawnOnSingleLive());
            CancelInvokes();
        }

        if (col.tag == "Fire02")
        {
            Destroy(col.gameObject);
            CancelInvoke("PlayerBurst01");
            CancelInvoke("InstantiateFire01");
            InvokeRepeating("PlayerBurst02", .2f, .2f);
            InvokeRepeating("InstantiateFire02", Random.Range(15, 20), Random.Range(15, 20));
        }

        if (col.tag == "Fire03")
        {
            Destroy(col.gameObject);
            CancelInvoke("PlayerBurst02");
            CancelInvoke("InstantiateFire02");
            InvokeRepeating("PlayerBurst03", .2f, .2f);
            InvokeRepeating("InstantiateFire03", Random.Range(15, 20), Random.Range(15, 20));
        }

        if (col.tag == "Fire04")
        {
            Destroy(col.gameObject);
            CancelInvoke("PlayerBurst03");
            CancelInvoke("InstantiateFire03");
            InvokeRepeating("PlayerBurst04", .2f, .2f);
            InvokeRepeating("InstantiateFire04", Random.Range(15, 20), Random.Range(15, 20));
        }

        if (col.tag == "Fire05")
        {
            Destroy(col.gameObject);
            CancelInvoke("PlayerBurst04");
            CancelInvoke("InstantiateFire04");
            InvokeRepeating("PlayerBurst05", .2f, .2f);
            InvokeRepeating("InstantiateFire05", Random.Range(15, 20), Random.Range(15, 20));
        }

        if (col.tag == "Fire06")
        {
            Destroy(col.gameObject);
            CancelInvoke("PlayerBurst05");
            CancelInvoke("InstantiateFire05");
            InvokeRepeating("PlayerBurst06", .2f, .2f);
            InvokeRepeating("InstantiateFire06", Random.Range(15, 20), Random.Range(15, 20));
        }

        if (col.tag == "Fire07")
        {         
            Destroy(col.gameObject);
            CancelInvoke("PlayerBurst06");
            CancelInvoke("InstantiateFire06");
            InvokeRepeating("PlayerBurst07", .2f, .2f);
        }

        if (col.tag == "Live")
        {
            liveScoreValue++;
            Destroy(col.gameObject);
        }

        if (col.tag == "FireBullets")
        {
            Destroy(col.gameObject);
            fireBullets.GetComponent<SpriteRenderer>().sprite = fire;
            GameManager.Instance.FireSound();
        }

        if (col.tag == "ArrowBullets")
        {
            Destroy(col.gameObject);
            fireBullets.GetComponent<SpriteRenderer>().sprite = arrow;
            GameManager.Instance.ArrowSound();
        }

        if (col.tag == "DiamondBullets")
        {
            Destroy(col.gameObject);
            fireBullets.GetComponent<SpriteRenderer>().sprite = diamond;
            GameManager.Instance.DiamondSound();
        }
    }

    public void CancelInvokes()
    {
        CancelInvoke("PlayerBurst01");
        CancelInvoke("PlayerBurst02");
        CancelInvoke("PlayerBurst03");
        CancelInvoke("PlayerBurst04");
        CancelInvoke("PlayerBurst05");
        CancelInvoke("PlayerBurst06");
        CancelInvoke("PlayerBurst07");
        CancelInvoke("InstantiateFire01");
        CancelInvoke("InstantiateFire02");
        CancelInvoke("InstantiateFire03");
        CancelInvoke("InstantiateFire04");
        CancelInvoke("InstantiateFire05");
        CancelInvoke("InstantiateFire06");
        CancelInvoke("InstantiateFire07");
        InvokeRepeating("PlayerBurst01", .2f, .2f);
        InvokeRepeating("InstantiateFire01", Random.Range(15, 20), Random.Range(15, 20));
    }

    void InstantiateFire01()
    {
        if (gameOver == false)
        {
            float randomPosX = Random.Range(-1.5f, 1.5f);
            Vector3 spawnPos = new Vector3(randomPosX, 5.5f, 0);
            GO1 = Instantiate(firePrefabs[1], spawnPos, firePrefabs[1].transform.rotation);
        }
    }

    void InstantiateFire02()
    {
        if (gameOver == false)
        {
            float randomPosX = Random.Range(-1.5f, 1.5f);
            Vector3 spawnPos = new Vector3(randomPosX, 5.5f, 0);
            GO2 = Instantiate(firePrefabs[2], spawnPos, firePrefabs[2].transform.rotation);
        }
    }

    void InstantiateFire03()
    {
        if (gameOver == false)
        {
            float randomPosX = Random.Range(-1.5f, 1.5f);
            Vector3 spawnPos = new Vector3(randomPosX, 5.5f, 0);
            GO3 = Instantiate(firePrefabs[3], spawnPos, firePrefabs[3].transform.rotation);
        }
    }

    void InstantiateFire04()
    {
        if (gameOver == false)
        {
            float randomPosX = Random.Range(-1.5f, 1.5f);
            Vector3 spawnPos = new Vector3(randomPosX, 5.5f, 0);
            GO4 = Instantiate(firePrefabs[4], spawnPos, firePrefabs[4].transform.rotation);
        }
    }

    void InstantiateFire05()
    {
        if (gameOver == false)
        {
            float randomPosX = Random.Range(-1.5f, 1.5f);
            Vector3 spawnPos = new Vector3(randomPosX, 5.5f, 0);
            GO5 = Instantiate(firePrefabs[5], spawnPos, firePrefabs[5].transform.rotation);
        }
    }

    void InstantiateFire06()
    {
        if (gameOver == false)
        {
            float randomPosX = Random.Range(-1.5f, 1.5f);
            Vector3 spawnPos = new Vector3(randomPosX, 5.5f, 0);
            GO6 = Instantiate(firePrefabs[6], spawnPos, firePrefabs[6].transform.rotation);
        }
    }

    public void PlayerBurst01()
    {
        if (gameOver == false)
            StartCoroutine(FireBurstPlayerFire01(fireBullets, 1, 100));
            //StartCoroutine(FireBurstPlayerFire01(fireBullets, 10, 700));
    }

    public void PlayerBurst02()
    {
        if (gameOver == false)
            StartCoroutine(FireBurstPlayerFire02(fireBullets, 1, 100));
        //StartCoroutine(FireBurstPlayerFire02(fireBullets, 10, 700));
    }

    public void PlayerBurst03()
    {
        if (gameOver == false)
            StartCoroutine(FireBurstPlayerFire03(fireBullets, 1, 100));
        //StartCoroutine(FireBurstPlayerFire03(fireBullets, 10, 700));
    }

    public void PlayerBurst04()
    {
        if (gameOver == false)
            StartCoroutine(FireBurstPlayerFire04(fireBullets, 1, 100));
        //StartCoroutine(FireBurstPlayerFire04(fireBullets, 10, 700));
    }

    public void PlayerBurst05()
    {
        if (gameOver == false)
            StartCoroutine(FireBurstPlayerFire05(fireBullets, 1, 100));
        //StartCoroutine(FireBurstPlayerFire05(fireBullets, 10, 700));
    }

    public void PlayerBurst06()
    {
        if (gameOver == false)
            StartCoroutine(FireBurstPlayerFire06(fireBullets, 1, 100));
        //StartCoroutine(FireBurstPlayerFire06(fireBullets, 10, 700));
    }

    public void PlayerBurst07()
    {
        if (gameOver == false)
            StartCoroutine(FireBurstPlayerFire07(fireBullets, 1, 100));
        //StartCoroutine(FireBurstPlayerFire07(fireBullets, 10, 700));
    }

    public IEnumerator FireBurstPlayerFire01(GameObject bulletPrefab, int burstSize, float rateOfFire)
    {
        float bulletDelay = 60 / rateOfFire;
        for (int i = 0; i < burstSize; i++)
        {
            {
                Instantiate(fireBullets, attackPoint01.position, Quaternion.identity);

                yield return new WaitForSeconds(bulletDelay);
            }
        }
    }

    public IEnumerator FireBurstPlayerFire02(GameObject bulletPrefab, int burstSize, float rateOfFire)
    {
        float bulletDelay = 60 / rateOfFire;
        for (int i = 0; i < burstSize; i++)
        {
            Instantiate(fireBullets, attackPoint02.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint03.position, Quaternion.identity);

            yield return new WaitForSeconds(bulletDelay);
        }
    }

    public IEnumerator FireBurstPlayerFire03(GameObject bulletPrefab, int burstSize, float rateOfFire)
    {
        float bulletDelay = 60 / rateOfFire;
        for (int i = 0; i < burstSize; i++)
        {
            Instantiate(fireBullets, attackPoint01.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint02.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint03.position, Quaternion.identity);

            yield return new WaitForSeconds(bulletDelay);
        }
    }

    public IEnumerator FireBurstPlayerFire04(GameObject bulletPrefab, int burstSize, float rateOfFire)
    {
        float bulletDelay = 60 / rateOfFire;
        for (int i = 0; i < burstSize; i++)
        {
            Instantiate(fireBullets, attackPoint02.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint03.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint11.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint12.position, Quaternion.identity);

            yield return new WaitForSeconds(bulletDelay);
        }
    }

    public IEnumerator FireBurstPlayerFire05(GameObject bulletPrefab, int burstSize, float rateOfFire)
    {
        float bulletDelay = 60 / rateOfFire;
        for (int i = 0; i < burstSize; i++)
        {
            Instantiate(fireBullets, attackPoint01.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint02.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint03.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint06.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint07.position, Quaternion.identity);

            yield return new WaitForSeconds(bulletDelay);
        }
    }

    public IEnumerator FireBurstPlayerFire06(GameObject bulletPrefab, int burstSize, float rateOfFire)
    {
        float bulletDelay = 60 / rateOfFire;
        for (int i = 0; i < burstSize; i++)
        {
            Instantiate(fireBullets, attackPoint02.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint03.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint11.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint12.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint09.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint10.position, Quaternion.identity);            

            yield return new WaitForSeconds(bulletDelay);
        }
    }

    public IEnumerator FireBurstPlayerFire07(GameObject bulletPrefab, int burstSize, float rateOfFire)
    {
        float bulletDelay = 60 / rateOfFire;
        for (int i = 0; i < burstSize; i++)
        {
            Instantiate(fireBullets, attackPoint02.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint03.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint11.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint12.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint13.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint09.position, Quaternion.identity);
            Instantiate(fireBullets, attackPoint10.position, Quaternion.identity);

            yield return new WaitForSeconds(bulletDelay);
        }
    }
}
